FROM gcc:latest

# Copy the source code to the container
COPY . /app

# Set the working directory
WORKDIR /app

# Compile the code
RUN g++ -c -Wall -Werror -fpic CarEngine.cpp
RUN g++ -shared -o libCarEngine.so CarEngine.o

# Set the environment variable for the library path
ENV LD_LIBRARY_PATH /app

# Start the container with a default command to run the test
CMD ["./CarEngine_test"]

