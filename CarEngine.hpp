#ifndef CAR_ENGINE_HPP
#define CAR_ENGINE_HPP

class CarEngine {
private:
    float engineDisplacement;
    int numCylinders;
    int maxRpm;
    int currentRpm;

public:
    CarEngine(float displacement, int cylinders, int maxRpm);
    void startEngine();
    void accelerate(int rpmIncrease);
    void decelerate(int rpmDecrease);
    void displayEngineStatus();
};

#endif /* CAR_ENGINE_HPP */

