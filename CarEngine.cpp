#include <iostream>
#include "CarEngine.hpp"

CarEngine::CarEngine(float displacement, int cylinders, int maxRpm) {
    engineDisplacement = displacement;
    numCylinders = cylinders;
    this->maxRpm = maxRpm;
    currentRpm = 0;
}

void CarEngine::startEngine() {
    std::cout << "Starting engine..." << std::endl;
    currentRpm = 1000;
}

void CarEngine::accelerate(int rpmIncrease) {
    currentRpm += rpmIncrease;
    if (currentRpm > maxRpm) {
        currentRpm = maxRpm;
    }
}

void CarEngine::decelerate(int rpmDecrease) {
    currentRpm -= rpmDecrease;
    if (currentRpm < 0) {
        currentRpm = 0;
    }
}

void CarEngine::displayEngineStatus() {
    std::cout << "Engine status: " << std::endl;
    std::cout << "Engine displacement: " << engineDisplacement << std::endl;
    std::cout << "Number of cylinders: " << numCylinders << std::endl;
    std::cout << "Max RPM: " << maxRpm << std::endl;
    std::cout << "Current RPM: " << currentRpm << std::endl;
}

