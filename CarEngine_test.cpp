#include <gtest/gtest.h>
#include "CarEngine.hpp"

TEST(CarEngineTest, EngineStartsAt1000RPM) {
    CarEngine myEngine(2.0, 4, 6000);
    myEngine.startEngine();
    EXPECT_EQ(myEngine.getCurrentRpm(), 1000);
}

TEST(CarEngineTest, EngineAcceleratesCorrectly) {
    CarEngine myEngine(2.0, 4, 6000);
    myEngine.startEngine();
    myEngine.accelerate(2000);
    EXPECT_EQ(myEngine.getCurrentRpm(), 3000);
}

TEST(CarEngineTest, EngineDeceleratesCorrectly) {
    CarEngine myEngine(2.0, 4, 6000);
    myEngine.startEngine();
    myEngine.accelerate(2000);
    myEngine.decelerate(1000);
    EXPECT_EQ(myEngine.getCurrentRpm(), 2000);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

